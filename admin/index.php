<?php

    session_start();
        
        if ( !isset($_SESSION['user']) ) {
            header('Location: signin.php');
        }

?>
<!DOCTYPE html>
<html lang="en">

<?php include('header.php'); ?>

  <body>

<?php include('nav.php'); ?>

<?php 
require($_SERVER['DOCUMENT_ROOT'] . "/app/MTGApp.php");
    
    if ( isset($_GET['function']) ) {
        switch($_GET['function']) {
            case 'inventory':
                if ( isset($_GET['cmd'])) {
                    switch($_GET['cmd']) {
                        case 'search':
                            include 'http://magic.isleofgamesaz.com/app/search.php';
                            break;
                        
                        case 'browse':
                            include 'http://magic.isleofgamesaz.com/app/browse.php';
                            break;
                        
                        default:
                            break;
                    }
                }
                else {
                    header('Location: index.php');
                }
                
                break;
            case 'transaction':
                
                
                break;
            
            case 'config':
                if ( isset($_GET['cmd'])) {
                    switch($_GET['cmd']) {
                        case 'cond':
                            
                            echo '<div id="cond_container" class="panel panel-default" style="margin-left: 5%; width: 90%">
                                    <div class="panel-heading">Condition</div>
                                    <div class="panel-body" id="cond_content">
                            ';
                            
                            $data = array(
                                'javascript' => 'no'
                            );
                            
                            $app = new MTGApp($data);
                            
                            if ( count($conditions = $app->getConditions()) == 0 ) {
                                echo "<h4>No conditions are currently configured.</h4>";
                            }
                            else {
                                echo '<table id="conditions" class="table table-striped table-hover">';
                                echo '<tbody><tr><th>Code</th><th>Name</th><th>Description</th><th>Reduction %</th><th style="width: 300px;"></th></tr>';
                                foreach( $conditions as $cond ) {
                                    echo '<tr id = "' . $cond['id'] . '"><td>' . $cond['code'] . '</td><td>' . $cond['name'] . '</td><td>' . $cond['description'] . '</td><td>' . $cond['percent'] . '</td>';
                                    echo '<td><form method="post" action="../app/admin.php"><input type="hidden" name="javascript" value="no" /><input type="hidden" name="function" value="condition" /><input type="hidden" name="cmd" value="delete" /><input type="hidden" name="id" value="' . $cond['id'] . '" /><button class="cond_delete_button" btn btn-primary" type="submit">Delete</button><button type="button" class="cond_delete_button" btn btn-primary" onclick="openCondModal(\'' . $cond['id'] . '\', $(\'#add_cond_form\').clone().attr(\'id\',\'mod_cond_form\'))">Modify</button></form></td>';

                                    echo '</tr>';
                                }
                                echo '</tbody></table>';
                            }
                            echo '<h3>Add a Condition</h3>';
                            echo '<form class="form-horizontal form-inline container" id="add_cond_form" method="post" action="../app/admin.php">';
                            echo '<input type="hidden" id="cond_js" name="javascript" value="no" />';
                            echo '<input type="hidden" id="cond_function" name="function" value="condition" />';
                            echo '<input type="hidden" id="cond_cmd" name="cmd" value="add" />';
                            echo '<div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="cond_name">Name</label>
                                        <input class="form-control input-sm" type="text" id="cond_name" name="name" value="" required>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="cond_code">Code</label>                    
                                        <input class="form-control input-sm" type="text" id="cond_code" name="code" length="2"  value="" required>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="cond_percent">Reduction</label>
                                        <div class="input-group">
                                           <input class="form-control input-sm" type="number" id="cond_percent" name="percent" max="100" min="0"  value="" required>
                                           <span class="input-group-addon input-sm">%</span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label for="cond_description" class="col-lg-2 control-label">Description</label>
                                        <input class="form-control input-sm" length="1024" id="cond_description" name="description"  value="">
                                     
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-lg-10 col-lg-offset-2">
                                      <br />
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                  </div>
                                    
                                </div>';
                            echo'   </div>
                                </div>
                            ';
                            echo '
                            <div id="cond_modal" class="modal">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div id="modal_header" class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h2 class="modal-title">Modify Condition</h2>
                                    </div>
                                    <div id="modal_content" class="modal-body">

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            ';
                            break;
                        
                        case 'lang':
                            echo '<div id="lang_container" class="panel panel-default" style="margin-left: 5%; width: 90%">
                                    <div class="panel-heading">Languages</div>
                                    <div class="panel-body" id="lang_content">
                            ';
                            
                            $data = array(
                                'javascript' => 'no'
                            );
                            
                            $app = new MTGApp($data);
                            
                            if ( count($languages = $app->getLanguages()) == 0 ) {
                                echo "<h4>No languages are currently configured.</h4>";
                            }
                            else {
                                echo '<table id="languages" class="table table-striped table-hover">';
                                echo '<tbody><tr><th>Code</th><th>Name</th><th>Markup %</th><th style="width: 300px;"></th></tr>';
                                foreach( $languages as $lang ) {
                                    echo '<tr id = "' . $lang['id'] . '"><td>' . $lang['code'] . '</td><td>' . $lang['name'] . '</td><td>' . $lang['percent'] . '</td>';
                                    echo '<td><form method="post" action="../app/admin.php"><input type="hidden" name="javascript" value="no" /><input type="hidden" name="function" value="language" /><input type="hidden" name="cmd" value="delete" /><input type="hidden" name="id" value="' . $lang['id'] . '" /><input class="lang_delete_button" btn btn-primary" type="submit" value="Delete"><button type="button" class="lang_delete_button" btn btn-primary" onclick="openLangModal(\'' . $lang['id'] . '\', $(\'#add_lang_form\').clone().attr(\'id\',\'mod_lang_form\'))">Modify</button></form></td>';

                                    echo '</tr>';
                                }
                                echo '</tbody></table>';
                            }
                            echo '<h3>Add a Language</h3>';
                            echo '<form class="form-horizontal form-inline container" id="add_lang_form" method="post" action="../app/admin.php">';
                            echo '<input type="hidden" id="lang_js" name="javascript" value="no" />';
                            echo '<input type="hidden" id="lang_function" name="function" value="language" />';
                            echo '<input type="hidden" id="lang_cmd" name="cmd" value="add" />';
                            echo '<div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="lang_name">Name</label>
                                        <input class="form-control input-sm" type="text" id="lang_name" name="name" value="" required>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="lang_code">Code</label>                    
                                        <input class="form-control input-sm" type="text" id="lang_code" name="code" length="2"  value="" required>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="lang_percent">Markup</label>
                                        <div class="input-group">
                                           <input class="form-control input-sm" type="number" id="lang_percent" name="percent" max="100" min="0"  value="" required>
                                           <span class="input-group-addon input-sm">%</span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-lg-10 col-lg-offset-2">
                                      <br />
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                  </div>
                                    
                                </div>';
                            echo'   </div>
                                </div>
                            ';
                            echo '
                            <div id="lang_modal" class="modal">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div id="modal_header" class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h2 class="modal-title">Modify Language</h2>
                                    </div>
                                    <div id="modal_content" class="modal-body">

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            ';
                            break;
                            
                    case 'group':
                            echo '<div id="group_container" class="panel panel-default" style="margin-left: 5%; width: 90%">
                                    <div class="panel-heading">Groups</div>
                                    <div class="panel-body" id="group_content">
                            ';
                            
                            
                            $data = array(
                                'javascript' => 'no'
                            );
                            
                            $app = new MTGApp($data);
                            
                            if ( count($groups = $app->getGroups()) == 0 ) {
                                echo "<h4>No groups are currently configured.</h4>";
                            }
                            else {
                                echo '<table id="groups" class="table table-striped table-hover">';
                                echo '<tbody><tr><th>Name</th><th>Description</th><th>Default</th><th style="width: 300px;"></th></tr>';
                                foreach( $groups as $group ) {
                                    $default = [ '<button type="button" class="group_default_button" btn btn-primary" onclick="makeDefault(\'' . $group['id'] . '\')">Make Default</button>', "Default" ];                     
                                    
                                    echo '<tr id= "' . $group['id'] . '"><td>' . $group['name'] . '</td><td>' . $group['description'] . '</td><td>' . $default[$group['default']] . '</td>';
                                    
                                    echo '<td><form method="post" action="../app/admin.php"><input type="hidden" name="javascript" value="no" /><input type="hidden" name="function" value="group" /><input type="hidden" name="cmd" value="delete" /><input type="hidden" name="id" value="' . $group['id'] . '" /><input class="group_delete_button" btn btn-primary" type="submit" value="Delete"><button type="button" class="group_delete_button" btn btn-primary" onclick="openGroupModal(\'' . $group['id'] . '\', $(\'#add_group_form\').clone().attr(\'id\',\'mod_group_form\'))">Modify</button></form></td>';

                                    echo '</tr>';
                                }
                                echo '</tbody></table>';
                            }
                            echo '<h3>Add a Group</h3>';
                            echo '<form class="form-horizontal form-inline container" id="add_group_form" method="post" action="../app/admin.php">';
                            echo '<input type="hidden" id="group_js" name="javascript" value="no" />';
                            echo '<input type="hidden" id="group_function" name="function" value="group" />';
                            echo '<input type="hidden" id="group_cmd" name="cmd" value="add" />';
                            echo '<div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="group_name">Name</label>
                                        <input class="form-control input-sm" type="text" id="group_name" name="name" value="" required>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-xs-12 col-sm-6 col-md-8 column">
                                        <label class="control-label" for="group_description">Description</label>                    
                                        <input class="form-control input-sm" type="text" id="group_description" name="description" length="2"  value="" required>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-lg-10 col-lg-offset-2">
                                      <br />
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                  </div>
                                    
                                </div>';
                            echo'   </div>
                                </div>
                            ';
                            echo '
                            <div id="group_modal" class="modal">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div id="modal_header" class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h2 class="modal-title">Modify Group</h2>
                                    </div>
                                    <div id="modal_content" class="modal-body">

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            ';
                            break;
                         
                        case 'accounts':

                            break;
                        
                        default:
                            
                            break;
                    }
                }                
                
                break;
            
            default:
                break;
            
        }
    }
    else if ( isset($_GET['error']) ) {
        echo '<div id="error_container" class="panel panel-danger" style="margin-left: 5%; width: 90%">
                <div class="panel-heading">Error</div>
                <div class="panel-body" id="error_content">
        ';
        echo '<h3>' . $_GET['message'] . '</h3>';
        echo '<h5>' . $_GET['error'] , '</h5>';
        echo'   </div>
            </div>
        ';
    }
    
    else {
        echo "<br />DASHBOARD!<br />";
    }



?>



<?php include('footer.php'); ?>

  </body>
</html>