    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar">TEST</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">MTG Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
        <li class="dropdown">
            <a id="inventory_nav" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Inventory <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="index.php?function=inventory&cmd=search">Search</a></li>
                <li><a href="index.php?function=inventory&cmd=browse">Browse</a></li>
            </ul>
        </li>
        <li><a id="transaction_nav" href="index.php?function=transaction">Transaction</a></li>

        <li class="dropdown">
            <a id="inventory_nav" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Configuration <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="index.php?function=config&cmd=cond">Condition</a></li>
                <li><a href="index.php?function=config&cmd=lang">Language</a></li>
                <li><a href="index.php?function=config&cmd=group">Groups</a></li>
                <li><a href="index.php?function=config&cmd=accounts">Accounts</a></li>
            </ul>
        </li>
      </ul> 
            
          <form class="navbar-form navbar-right" method="get" action="app/results.php">
 
              
              
          </form>
            <div class="navbar-right"><span style="color: white;">Logged in as </span><a href="#"><?php echo $_SESSION['user']['firstname'] . " " . $_SESSION['user']['lastname']; if ( $_SESSION['user']['admin'] == 1 ) { echo " (admin)"; } ?> | <a href="signout.php">Log out</a> 
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

