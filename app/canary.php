<?php

// Function to generate a random string of the passed length
function generate_salt($length)
{
    $characters = 'ABCDEFGHJIKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^*_-`';
    $lastchar = strlen($characters) - 1;

    $salt = '';
    for ($i = 0; $i < $length; $i++)
    {
        $index = mt_rand(0, $lastchar);
        $character = $characters{$index};
        $salt .= $character;
    }
    return $salt;
}


// CSRF prevention: if there isn't a CSRF token for this session yet, make one!
function ensure_csrf_token_exists($force = false)
{
    if (!isset($_SESSION['canary']) || $force)
    {
        // Base64 encode the canary for the current session, this
        // makes sure that the canary not composed of special
        // characters while still making it virtually impossible to forge
        $_SESSION['canary'] = base64_encode(generate_salt(64));
    }

}

// There should always be a canary
ensure_csrf_token_exists();

?>