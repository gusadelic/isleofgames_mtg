<?php

    session_name("mtg");
    session_start();


	if ( isset($_GET['cmd']) ) {

		require("SearchResult.php");
		//echo("USING THE CLASS!<br />\n");
		$search = new SearchResult( $_GET['search'], $_GET['javascript'], $_GET['cmd']);


		if ( strcmp("yes", $_GET['javascript']) == 0 ) {
			$output = json_encode($search->result);
			$output = str_replace("`", "'", $output);
			echo $output;
		}
		else {
			echo "Results: <br />";
			if ( isset($search->result) ) {
                            foreach ($search->result as $result) {
				$cardname = utf8_decode($result['name']);
                                echo "<a href=\"results.php?card=" . $cardname . "&canary=" . $_SESSION['canary'] . "\">" . $cardname . "</a><br />";
                            }
                        }
                }
        }

	else if ( isset( $_GET['search'] ) ) {
		require("global.inc.php");
		require("functions.php");
		getResult( str_replace('\\', "", $_GET['search']), $_GET['javascript'], $db ); 

	}


	else if ( isset( $_GET['id'] ) ) {
		require("global.inc.php");
		require("functions.php");
		getCard( $_GET['id'], $_GET['javascript'], $db ); 
		
	}

	else if ( isset( $_GET['set'] ) ) {
		require("global.inc.php");
		require("functions.php");
		getSetList( $_GET['set'], $_GET['javascript'], $db );

	}

	else if ( isset( $_GET['setname'] ) ) {
		require("global.inc.php");
		require("functions.php");
		getSetName( $_GET['setname'], $_GET['javascript'], $db );
	}
        
        else if ( isset( $_GET['setdata'] ) ) {
		require("global.inc.php");
		require("functions.php");
		getSetData( $_GET['setdata'], $_GET['javascript'], $db );
	}

	else if ( isset ( $_GET['stock-card'] ) ) {
		require("global.inc.php");
		require("functions.php");
		getStockCard( $_GET['stock-card'], $_GET['javascript'], $db );
	}

	else if ( isset ( $_GET['stock-set'] ) ) {
		require("global.inc.php");
		require("functions.php");
		getStockSet( $_GET['stock-set'], $_GET['javascript'], $db );
	}
        else if ( isset ( $_GET['pricelist'] ) ) {
		require("global.inc.php");
		require("functions.php");
                getPriceList( $_GET['pricelist'], $_GET['foil'], $_GET['javascript'], $db );
	}
        else if ( isset ( $_GET['price'] ) ) {
		require("global.inc.php");
		require("functions.php");
                getPrice( $_GET['price'], $_GET['foil'], $_GET['javascript'], $db );
	}

	else { 
		require("global.inc.php");
		require("functions.php");
			//RETURN ALL CARDS
    	$get_search_query = $db->prepare("
        	SELECT * FROM cards 
			ORDER BY id DESC
       	 ");

   		$get_search_query->execute();

		$results = $get_search_query->fetchAll();

		if ( strcmp("yes", $_GET['javascript']) == 0 ) {
			$output = json_encode($results);
			$output = str_replace("`", "'", $output);
			echo $output;
		}
		else {
			echo "Results: <br />";
      	        foreach ($results as $result) {
					$cardname = utf8_decode($result['name']);
                    echo '<a href="results.php?card='. $cardname . '">'. $cardname . '</a><br />';
				}
		}

	}








?>

