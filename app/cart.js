function viewcart() {

	$("#result").html("<h1>Shopping Cart</h1>");
	$("#result").append("<table><tbody><tr><th>Item</th><th>Quantity</th><th>Price</th><th> </th></tr></tbody></table>");

					$.ajax({
                		url: 'cart.php?cmd=view&canary=' + $('#canary').val(),
                		type: 'GET',
                   	dataType: 'JSON',
                		async: false,
                		success: function(data) {			
                      printcart(data.cart);
                			},
      					    error:function(xhr, ajaxOptions, thrownError){

              				console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
              				if ( xhr.status = 200 ) {
              					$("#result tbody").append('<tr><td colspan="4">' + xhr.responseText + '</td></tr>');
              				}

              			}
             		});
        }


function printcart(items) {
    var subtotal = 0;
    var cart_number = 0;
                      $.each(items, function(i, v) {
                        var product = items[i].quantity * items[i].amount;
                        var cartitem = "cartitem_" + ++cart_number;
                          $("#dialog_cart tbody").append('<tr id="' + cartitem + '"></tr>'); 
                          subtotal = subtotal + product;
                          $("#" + cartitem  + "").append('<td>' + items[i].item_name + '</td>');
                          $("#" + cartitem  + "").append('<td>' + items[i].quantity + '</td>');
                          $("#" + cartitem  + "").append('<td>' + accounting.formatMoney(items[i].amount) + '</td>');
                          $("#" + cartitem  + "").append('<td>' + accounting.formatMoney(product) + '</td>');
                          $("#" + cartitem  + "").append('<td><input type="number" step="1" min="1" id="remove_qty" class="qty" autocomplete="off" required /></input><input type="submit" class="listbutton" onClick="change_qty(' + cart_number + ')" value="Remove"></input></td>');
                          
                        });

                      subtotal = subtotal + parseInt(items[0].shipping);
                     
                      $('#dialog_cart tbody').append('<tr class="shipping"><td> </td><td colspan="2">Shipping</td><td>' + accounting.formatMoney(items[0].shipping) + '</td><td> </td></tr>');
                      $('#dialog_cart tbody').append('<tr class="total"><td> </td><td colspan="2">Total</td><td>' + accounting.formatMoney(subtotal) + '</td><td> <input id="clear_button" type="button" onClick="clear_cart()" value="Clear Cart" /> </td></tr>');

                                $.ajax({
                                          url: 'checkout.php',
                                          type: 'POST',
                                          async: false,

                                          success: function(paypal) {
                                            $('#dialog_cart').append('<div class="cart_buttons">' + paypal + '</div>');
                                            },
                                          
                                          error:function(xhr, ajaxOptions, thrownError){

                                            console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);

                                             }
                                      });
}


function view_dialog_cart() {

                    $('#dialog_cart').attr('title',"Shopping Cart");
                    $("#dialog_cart").dialog({ modal: true, height: 350, width: 800});

                    dialog_cart();
}




function dialog_cart() {

  $("#dialog_cart").html("<h1>Shopping Cart</h1>");
  $("#dialog_cart").append('<table class="shopping_cart"><tbody><tr><th class="cart_item">Item</th><th class="cart_qty">Quantity</th><th class="cart_price">Price</th><th class="cart_total"> </th><th class="cart_modify"> </th></tr></tbody></table>');

          $.ajax({
                    url: 'cart.php?cmd=view&canary=' + $('#canary').val(),
                    type: 'GET',
                    dataType: 'JSON',
                    async: false,
                    success: function(data) {    
                      printcart(data.cart);
                      },
                	  error:function(xhr, ajaxOptions, thrownError){
                      console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
                      if ( xhr.status = 200 ) {
                        $("#dialog_cart").html('<h2>' + xhr.responseText + '</h2>');
                      }
                    }
          });
}


function clear_cart() {

         $.ajax({
                    url: 'cart.php?cmd=clear&canary=' + $('#canary').val(),
                    type: 'GET',
                    async: false,

                    success: function(data) {
                      $('#dialog_cart').dialog( "destroy" );
                      view_dialog_cart();
                      },  
                    
                    error:function(data, ajaxOptions, thrownError){

                      console.log('status = '+ data.status +' thrownError = '+ thrownError + ' response = '+ data.responseText);

                       }
                });

}

function change_qty(item) {

               $.ajax({
                    url: 'cart.php?cmd=modify&qty=' + $("#cartitem_" + item + " #remove_qty").val() + "" + '&item=' + item + '&canary=' + $('#canary').val(),
                    type: 'GET',
                    async: false,

                    success: function(data) {
                      $('#dialog_cart').dialog( "destroy" );
                      view_dialog_cart();
                      },  
                    
                    error:function(data, ajaxOptions, thrownError){

                      console.log('status = '+ data.status +' thrownError = '+ thrownError + ' response = '+ data.responseText);

                       }
                });

  $("form").submit(function() {
    return false;
  });
}

function login_dialog() {
  $('#dialog_cart').attr('title',"Login");
    $("#dialog_cart").dialog({ modal: true, height: 400, width: 500});

              $.ajax({
                    url: 'login.php',
                    type: 'GET',
                    dataType: 'JSON',
                    async: false,
                    success: function(data) {    
                      $("#dialog_cart").html(data.cart);
                      },
                    error:function(xhr, ajaxOptions, thrownError){
                      console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
                      if ( xhr.status = 200 ) {
                        $("#dialog_cart").html('<h2>' + xhr.responseText + '</h2>');
                      }
                    }
          });
}

function newuser_dialog() {
  $('#dialog_cart').attr('title',"New User:");
    $("#dialog_cart").dialog({ modal: true, height: 500, width: 500});

              $.ajax({
                    url: 'newuser.php',
                    type: 'GET',
                    async: false,
                    success: function(data) {    
                      $("#dialog_cart").html(data);
                      },
                    error:function(xhr, ajaxOptions, thrownError){
                      console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
                      if ( xhr.status = 200 ) {
                        $("#dialog_cart").html('<h2>' + xhr.responseText + '</h2>');
                      }
                    }
          });
}