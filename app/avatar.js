$( document ).ready(function() {
	$("#javascript").val("yes");
	$("#avatar_search").each(function(){
		$(this).attr("autocomplete", "off");
	});

	$("#avatar_search").keyup( function(event) {

        	$.get('results.php', $("#avatarform").serialize(), function(result) {
			
			var json = JSON.parse(result);

			$("#avatar_results").html("");

			$.each(json, function(index, value) {
                    $("#avatar_results").append('<li class="result">' + json[index].name + '</li>');
                });

			$("#avatar_results").css({border: '2px solid black'});
			$("#avatar_results").css({background: '#f1f1f1'});

			if ( $("#avatar_search").val() === "" ) {
					$("#avatar_results").css({border: '2px solid white'});
					$("#avatar_results").css({background: '#ffffff'});
                    $("#avatar_results").html("");
            }
			else if ( !json[0] ) {
				$("#avatar_results").html("No Matches!");
			}
		}); 

	});

	$("#avatarform").submit(function() {
		return false;
	});

});

    $(document).on("click", "li.result", function() {
            $("#avatar_search").val( $(this).html() );
            
            $("#avatar_results").css({border: '2px solid white'});
            $("#avatar_results").css({background: '#ffffff'});
            $("#avatar_results").html("");
    });

