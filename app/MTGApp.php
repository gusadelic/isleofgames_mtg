<?php


class MTGApp
{
        var $admin;     // boolean, 0 = not admin, 1 = admin
        var $data;      // An array of parameters
	var $db;        // The Database object
	var $js;        // boolean, js yes or no

        
	function __construct($data) {
                
                // setup the DB
                require 'global.inc.php';
                $this->db = $db;
		$this->js = $data['javascript'];
                if ( !isset($_SESSION['user']) ) {
                    $this->admin = 0;
                }
                else {
                    $this->admin = $_SESSION['user']['admin'];
                }
                
                $this->data = $data;
                
                if ( isset($this->data['function']) ) {   
                
                    if ( !isset($data['cmd']) ) { $this->data['cmd'] = ""; }
                                           
                    switch($this->data['function']) {
                        case 'cards':
                            switch($this->data['cmd']) {
                                case 'search':
                                    $this->getSearchResult();
                                    break;
                                case 'card':
                                    $this->getCard();
                                    break;
                                case 'set':
                                    $this->getSetCards();
                                    break;
                                case 'price':
                                    $this->getPrice();
                                    break;
                                case 'pricelist':
                                    $this->getPriceList();
                                    break;
                            }
                            break;
                        
                        case 'set':
                            switch($this->data['cmd']) {
                                case 'list':
                                   $this->getSetList();
                                   break;                            
                                case 'name':
                                    $this->getSetName();
                                    break;
                                case 'data':
                                    $this->getSetData();
                                    break;
                                default:
                                    $this->getSets();
                                    break;
                            }
                            break;
                                    

                        case 'inventory':
                            switch($this->data['cmd']) {
                                case 'modify':
                                    $this->modStock();
                                    break;
                                case 'add':
                                    $this->addStock();
                                    break;
                                case 'delete':
                                    $this->delStock();
                                    break;
                                case 'get':
                                    $this->getStock();
                                    break;
                                case 'set':
                                    $this->getSetStock();
                                    break;
                                default:
                                    $this->getAllStock();                         
                            }
                            break;

                        case 'language':
                            switch($this->data['cmd']) {
                                case 'add':
                                    $this->addLanguage();
                                    break;
                                case 'delete':
                                    $this->delLanguage();
                                    break;
                                case 'modify':
                                    $this->modLanguage();
                                    break;
                                case 'get':
                                    $this->getLanguage();
                                    break;
                                default:
                                    $this->getLanguages();               
                            }
                            break;

                        case 'condition':
                            switch($this->data['cmd']) {
                                case 'add':
                                    $this->addCondition();
                                    break;
                                case 'delete':
                                    $this->delCondition();
                                    break;
                                case 'modify':
                                    $this->modCondition();
                                    break;
                                case 'get':
                                    $this->getCondition();
                                    break;
                                default:
                                    $this->getConditions();                       
                            }
                            break;
                        
                        case 'group':
                            switch($this->data['cmd']) {
                                case 'add':
                                    $this->addGroup();
                                    break;
                                case 'delete':
                                    $this->delGroup();
                                    break;
                                case 'modify':
                                    $this->modGroup();
                                    break;
                                case 'get':
                                    $this->getGroup();
                                    break;
                                case 'default':
                                    $this->makeGroupDefault();
                                    break;
                                default:
                                    $this->getGroups();                       
                            }
                            break;
                        
                        case 'table':
                            switch($this->data['cmd']) {
                                case 'set':
                                    $this->getSetListData();
                                    break;                               
                                default:
                                    $this->getTable();
                                    break;
                            }
                        
                    }          
                    
                    
                }
                

	} //Contructor
        
        function setData($data) {
            $this->data = $data;
        }
        
        function addData($data) {
            $this->data = array_merge($this->data, $data);
        }
        
        function clearData() {
            $this->data = array();
        }
        
        function getData() {
            return $this->data;
        }
        
        function isAdmin() {
            return $this->admin;
        }
        
        function isJavascript() {
            return $this->js;
        }
        
        //Admin Functions
        
        function getCard() {
        	$params = array(
		':id' => $this->id
	);

	$get_card_query = $db->prepare("
		SELECT * FROM cards
		WHERE id = :id
	");
	$get_card_query->execute($params);

	$card_array = $get_card_query->fetchAll();

	if ( !($card_array) ) { die("Card not found"); }

	if ( strcmp("yes", $this->js) == 0 ) {
		$output = json_encode($card_array);
		str_replace("`", "'", $output );
		echo utf8_decode($output);
	}
	else {


            }
	}
        
        function getSetCards() {

            $params = array(
                    ':cardname' => utf8_encode($this->data['cardname'])
            );

            $get_allcards_query = $this->db->prepare("
                    SELECT * FROM cards
                    WHERE `name` = :cardname
                    ORDER BY `id` DESC
            ");

            $get_allcards_query->execute($params);

            $allcards_array = $get_allcards_query->fetchAll();

            if ( !($allcards_array) ) { die("Card not found"); }


            if ( strcmp("yes", $this->js) == 0 ) {
                    echo json_encode($allcards_array);
            }
            else {


            }

        }

        function getPriceList() {
            if ( !isset($this->data['code']) ) { die ('REQUIRED - "code"\n'); }
            if ( !isset($this->data['foil']) ) { die ('REQUIRED - "foil"\n'); }
            
            $params = array(
                ':set' => $this->data['code'],
                ':foil' => $this->data['foil']
            );

            $get_pricelist = $this->db->prepare('
                SELECT `prices`.* FROM `prices` 
                INNER JOIN `cards` ON `cards`.`id` = `prices`.`card_id` 
                WHERE `cards`.`set` = :set AND `prices`.`foil` = :foil 
            ');

            if ( $get_pricelist->execute($params) ) {
                $pricelist = $get_pricelist->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js, "yes")==0 ) {
                    echo json_encode($pricelist);
                }
                else {
                    return $pricelist;
                }
            }
            else {
               echo "Failed to load Prices\n" . $get_pricelist->errorInfo()[2] . "\n";
            } 
        
        }
        
        
        function getPrice() {
            if ( !isset($this->data['card_id']) ) { die ('REQUIRED - "card_id"\n'); }
            if ( !isset($this->data['foil']) ) { die ('REQUIRED - "foil"\n'); }
            
            $params = array(
                ':id' => $this->data['card_id'],
                ':foil' => $this->data['foil']
            );

            $get_pricelist = $this->db->prepare('
                SELECT * FROM `prices`
                WHERE `card_id` = :id AND `foil` = :foil
            ');

            if ( $get_pricelist->execute($params) ) {
                $pricelist = $get_pricelist->fetch(PDO::FETCH_ASSOC);
                echo json_encode($pricelist);
            }
            else {
               echo "Failed to load Prices\n" . $get_pricelist->errorInfo()[2] . "\n";
            } 
        
        }
       
        
        function makeGroupDefault() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }
            
            $remove_default = $this->db->prepare("
                   UPDATE `groups` SET `default` = 0
            ");
            
            if ( $remove_default->execute() ) {
                
                $params = array( ':id' => $this->data['id'] );
                
                $set_default = $this->db->prepare("
                    UPDATE `groups` SET `default` = 1
                    WHERE `id` = :id
                ");
                
                if ( $set_default->execute($params) ) {
                    if ( strcmp($this->js, "yes")==0 ) {
                        echo "Default Succeeded";
                    }
                    else {
                        header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=group');
                    }
                }
                else {
                    if ( strcmp($this->js, "yes")==0 ) {
                        echo "Failed to make Default\n" . $set_default->errorInfo()[2] . "\n";
                    }
                    else {
                        header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to make Default&error=" . $set_default->errorInfo()[2]);
                    }
                }                
            }
            else {
                if ( strcmp($this->js, "yes")==0 ) {
                    echo "Failed to remove Default\n" . $remove_default->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to remove Default&error=" . $remove_default->errorInfo()[2]);
                }
            }
            
            
            
        }
        
        
        function getGroups() {
            
            $get_group = $this->db->prepare('
                SELECT * FROM `groups`
            ');

            if ( $get_group->execute() ) {
                $group = $get_group->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo json_encode($group);
                }
                else {
                    return $group;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to load Groups\n" . $get_group->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to load Groups&error=" . $get_group->errorInfo()[2]);
                }
            } 
        }
        
        function getGroup() {
        if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }
            
            $params = array( ':id' => $this->data['id'] );
            $get_group = $this->db->prepare('
                SELECT * FROM `groups`
                WHERE `id` = :id
            ');

            if ( $get_group->execute($params) ) {
                $group = $get_group->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo json_encode($group);
                }
                else {
                    return $group;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to load Group\n" . $get_group->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to load Group&error=" . $get_group->errorInfo()[2]);
                }
            } 
        }
    
        function addGroup() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['name']) ) { die ('REQUIRED - "name"\n'); }
            else if ( !isset($this->data['description']) ) { die ('REQUIRED - "description"\n'); }
            
            $params = array(
                ':name' => $this->data['name'],
                ':description' => $this->data['description']
            );

            $add_group = $this->db->prepare("
                INSERT INTO `groups` (`name`, `description`) 
                VALUES (:name, :description)                    
            ");

            if ( $add_group->execute($params) ) {
                if ( strcmp($this->js, 'yes') == 0 ) {
                    echo "Group Added!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=group');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Add Group\n" . $add_group->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Add Group&error=" . $add_group->errorInfo()[2]);
                }
            } 
        }
        
        function delGroup() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }

            $params = array(
                ':id' => $this->data['id']
            );
            $del_group = $this->db->prepare("
                DELETE FROM `groups`
                WHERE `id` = :id
            ");

            if ( $del_group->execute($params) ) {
                if ( strcmp($this->js, 'yes') == 0 ) {
                    echo "Group Deleted!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=group');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Delete Group\n" . $del_group->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Delete Group&error=" . $del_group->errorInfo()[2]);
                }
            } 
        }

        function modGroup() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }
            else if ( !isset($this->data['name']) ) { die ('REQUIRED - "name"\n'); }
            else if ( !isset($this->data['description']) ) { die ('REQUIRED - "description"\n'); }
            
            $params = array(
                ':id' => $this->data['id'],
                ':name' => $this->data['name'],
                ':description' => $this->data['description']
            );

            $mod_group = $this->db->prepare("
                UPDATE `groups` SET `code`=:code, `name`=:name, `percent`=:percent, `description`=:description
                WHERE `id` = :id 
            ");

            if ( $mod_group->execute($params) ) {
                if ( strcmp($this->data->js, 'yes') == 0 ) {
                    echo "Group Updated!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=group');
                }
            }
            else {
                if ( strcmp($this->data->js,"yes") == 0 ) {
                    echo "Failed to Modify Group\n" . $mod_group->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Modify Group&error=" . $mod_group->errorInfo()[2]);
                }
            } 
        }
        
        
        function getConditions() {

            $get_cond = $this->db->prepare('
                SELECT * FROM `condition`
            ');

            if ( $get_cond->execute() ) {
                $cond = $get_cond->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo json_encode($cond);
                }
                else {
                    return $cond;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to load Conditions\n" . $get_cond->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to load Conditions&error=" . $get_cond->errorInfo()[2]);
                }
            } 
        }
        
        function getCondition() {
        if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }
            
            $params = array( ':id' => $this->data['id'] );
            $get_cond = $this->db->prepare('
                SELECT * FROM `condition`
                WHERE `id` = :id
            ');

            if ( $get_cond->execute($params) ) {
                $cond = $get_cond->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo json_encode($cond);
                }
                else {
                    return $cond;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to load Conditions\n" . $get_cond->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to load Conditions&error=" . $get_cond->errorInfo()[2]);
                }
            } 
        }
    
        function addCondition() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['name']) ) { die ('REQUIRED - "name"\n'); }
            else if ( !isset($this->data['code']) ) { die ('REQUIRED - "code"\n'); }
            else if ( !isset($this->data['percent']) ) { die ('REQUIRED - "percent"\n'); }
            else if ( !isset($this->data['description']) ) { die ('REQUIRED - "description"\n'); }
            
            $params = array(
                ':name' => $this->data['name'],
                ':code' => $this->data['code'],
                ':percent' => $this->data['percent'],
                ':description' => $this->data['description']
            );

            $add_cond = $this->db->prepare("
                INSERT INTO `condition` (`name`, `code`, `percent`, `description`) 
                VALUES (:name, :code, :percent, :description)                    
            ");

            if ( $add_cond->execute($params) ) {
                if ( strcmp($this->js, 'yes') == 0 ) {
                    echo "Condition Added!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=cond');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Add Condition\n" . $add_cond->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Add Condition&error=" . $add_cond->errorInfo()[2]);
                }
            } 
        }
        
        function delCondition() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }

            $params = array(
                ':id' => $this->data['id']
            );
            $del_cond = $this->db->prepare("
                DELETE FROM `condition`
                WHERE `id` = :id
            ");

            if ( $del_cond->execute($params) ) {
                if ( strcmp($this->js, 'yes') == 0 ) {
                    echo "Condition Deleted!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=cond');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Delete Condition\n" . $del_cond->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Delete Condition&error=" . $del_cond->errorInfo()[2]);
                }
            } 
        }

        function modCondition() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }
            else if ( !isset($this->data['name']) ) { die ('REQUIRED - "name"\n'); }
            else if ( !isset($this->data['code']) ) { die ('REQUIRED - "code"\n'); }
            else if ( !isset($this->data['percent']) ) { die ('REQUIRED - "percent"\n'); }
            else if ( !isset($this->data['description']) ) { die ('REQUIRED - "description"\n'); }
            
            $params = array(
                ':id' => $this->data['id'],
                ':code' => $this->data['code'],
                ':name' => $this->data['name'],
                ':percent' => $this->data['percent'],
                ':description' => $this->data['description']
            );

            $mod_cond = $this->db->prepare("
                UPDATE `condition` SET `code`=:code, `name`=:name, `percent`=:percent, `description`=:description
                WHERE `id` = :id 
            ");

            if ( $mod_cond->execute($params) ) {
                if ( strcmp($this->data->js, 'yes') == 0 ) {
                    echo "Condition Updated!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=cond');
                }
            }
            else {
                if ( strcmp($this->data->js,"yes") == 0 ) {
                    echo "Failed to Modify Condition\n" . $mod_cond->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Modify Condition&error=" . $mod_cond->errorInfo()[2]);
                }
            } 
        }
        
        function getLanguages() {
            
            $get_lang = $this->db->prepare('
                SELECT * FROM `language`
            ');

            if ( $get_lang->execute() ) {
                $lang = $get_lang->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo json_encode($lang);
                }
                else {
                    return $lang;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to load Languages\n" . $get_lang->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to load Languages&error=" . $get_lang->errorInfo()[2]);
                }
            } 
        }
        
        
        function getLanguage() {
            
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }
            
            $params = array( ':id' => $this->data['id'] );
            
            $get_lang = $this->db->prepare('
                SELECT * FROM `language`
                WHERE `id` = :id
            ');

            if ( $get_lang->execute($params) ) {
                $lang = $get_lang->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo json_encode($lang);
                }
                else {
                    return $lang;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to load Language\n" . $get_lang->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to load Language&error=" . $get_lang->errorInfo()[2]);
                }
            } 
        }
        
        function addLanguage() {
            
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['name']) ) { die ('REQUIRED - "name"\n'); }
            else if ( !isset($this->data['code']) ) { die ('REQUIRED - "code"\n'); }
            else if ( !isset($this->data['percent']) ) { die ('REQUIRED - "percent"\n'); }
            
            $params = array(
                ':name' => $this->data['name'],
                ':code' => $this->data['code'],
                ':percent' => $this->data['percent']
            );

            $add_lang = $this->db->prepare("
                INSERT INTO `language` (`name`, `code`, `percent`) 
                VALUES (:name, :code, :percent)
            ");

            if ( $add_lang->execute($params) ) {
                if ( strcmp($this->js, 'yes') == 0 ) {
                    echo "Language Added!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=lang');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Add Langiage\n" . $add_lang->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Add Language&error=" . $add_lang->errorInfo()[2]);
                }
            } 
        }
        
        
        function delLanguage($id, $js) {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }

            $params = array(
                ':id' => $this->data['id']
            );
            $del_lang = $this->db->prepare("
                DELETE FROM `language`
                WHERE `id` = :id
            ");

            if ( $del_lang->execute($params) ) {
                if ( strcmp($this->js, 'yes') == 0 ) {
                    echo "Language Deleted!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=lang');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Delete Language\n" . $del_lang->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Delete Language&error=" . $del_lang->errorInfo()[2]);
                }
            } 
        }
    
    
        function modLanguage() {
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['id']) ) { die ('REQUIRED - "id"\n'); }
            else if ( !isset($this->data['name']) ) { die ('REQUIRED - "name"\n'); }
            else if ( !isset($this->data['code']) ) { die ('REQUIRED - "code"\n'); }
            else if ( !isset($this->data['percent']) ) { die ('REQUIRED - "percent"\n'); }
            
            $params = array(
                ':id' => $this->data['id'],
                ':code' => $this->data['code'],
                ':name' => $this->data['name'],
                ':percent' => $this->data['percent']
            );

            $mod_lang = $this->db->prepare("
                UPDATE `language` SET `code`=:code, `name`=:name, `percent`=:percent
                WHERE `id` = :id 
            ");

            if ( $mod_lang->execute($params) ) {
                if ( strcmp($this->js, 'yes') == 0 ) {
                    echo "Language Updated!";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php?function=config&cmd=lang');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Modify Language\n" . $mod_lang->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Modify Language&error=" . $mod_lang->errorInfo()[2]);
                }
            } 
        }
        
        
        function modStock() {
            
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['item']) ) { die ('REQUIRED - "item"\n'); }
            else if ( !isset($this->data['qty']) ) { die ('REQUIRED - "qty"\n'); }
            else if ( !isset($this->data['price']) ) { die ('REQUIRED - "price"\n'); }
            
            $params = array(
                ':item' => $this->data['item'],
                ':qty' => $this->data['qty'],
                ':price' => $this->data['price']
            );

            $update_stock_query = $this->db->prepare("
                UPDATE `stock` SET `stock` = :qty, `price` = :price WHERE `id` = :item
            ");

            if ( $update_stock_query->execute($params) ) {
                if ( strcmp($this->js, "yes") == 0 ) {
                    echo "success";
                }
                else {
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Modify Stock\n" . $update_stock_query->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Modify Stock&error=" . $update_stock_query->errorInfo()[2]);
                }
            }  
        }
        
        function addStock() {
            
            if ( $this->admin == 0 ) { die("Restricted!"); }
            
            if ( !isset($this->data['card_id']) ) { die ('REQUIRED - "card_id"\n'); }
            else if ( !isset($this->data['stock']) ) { die ('REQUIRED - "stock"\n'); }
            else if ( !isset($this->data['price']) ) { die ('REQUIRED - "price"\n'); }
            else if ( !isset($this->data['foil']) ) { die ('REQUIRED - "foil"\n'); }
            else if ( !isset($this->data['condition']) ) { die ('REQUIRED - "condition"\n'); }
            else if ( !isset($this->data['language']) ) { die ('REQUIRED - "language"\n'); }
            else if ( !isset($this->data['group']) ) { die ('REQUIRED - "group"\n'); }
            
            $params = array(
                ':card_id' => $this->data['card_id'],
                ':stock' => $this->data['stock'],
                ':price' => $this->data['price'],
                ':foil' => $this->data['foil'],
                ':condition' => $this->data['condition'],
                ':language' => $this->data['language'],
                ':group' => $this->data['group']
            );

            $update_stock_query = $this->db->prepare("
                INSERT INTO `stock` ( `card_id`, `condition`, `language`, `group`, `foil`, `stock`, `price` ) VALUES ( :card_id, :condition, :language, :group, :foil, :stock, :price ) 
            ");

            if ( $update_stock_query->execute($params) ) {
                if ( strcmp($this->js, "yes") == 0 ) {
                    echo ("success");
                }
                else { 
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Add Stock\n" . $update_stock_query->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Add Stock&error=" . $update_stock_query->errorInfo()[2]);
                }
            } 
        }
        
        function delStock() {
            
            if ( !isset($this->data['item']) ) { die ('REQUIRED - "item"\n'); }
            
            $params = array(
                ':item' => $this->data['item'],
            );

            $update_stock_query = $this->db->prepare("
                 DELETE FROM `stock` WHERE `id` = :item;
            ");

            if ( $update_stock_query->execute($params) ) {
                if ( strcmp($this->js, "yes") == 0 ) {
                    echo ("success");
                }
                else { 
                    header('Location: http://magic.isleofgamesaz.com/admin/index.php');
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Delete Stock\n" . $update_stock_query->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Delete Stock&error=" . $update_stock_query->errorInfo()[2]);
                }
            }        
        }
        
        function getStock() {
            
            if ( !isset($this->data['card_id']) ) { die ('REQUIRED - "card_id"\n'); }
            
            $params = array(
                ':card' => $this->data['card_id'],
            );

            $update_stock_query = $this->db->prepare("
                 SELECT * FROM `stock` WHERE `card_id` = :card;
            ");

            if ( $update_stock_query->execute($params) ) {
                 $stock = $update_stock_query->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js, "yes") == 0 ) {
                    echo json_encode($stock);
                }
                else { 
                    return $stock;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Get Stock\n" . $update_stock_query->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Get Stock&error=" . $update_stock_query->errorInfo()[2]);
                }
            }        
        }
        
        function getSetStock() {

            if ( !isset($this->data['code']) ) { die ('REQUIRED - "set"\n'); }
            
            $params = array(
                ':set' => $this->data['code'],
            );

            $update_stock_query = $this->db->prepare("
                 SELECT `stock`.* FROM `stock` INNER JOIN `cards` ON `cards`.id = `stock`.card_id WHERE `cards`.set = :set;
            ");

            if ( $update_stock_query->execute($params) ) {
                 $stock = $update_stock_query->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js, "yes") == 0 ) {
                    echo json_encode($stock);
                }
                else { 
                    return $stock;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Get Stock\n" . $update_stock_query->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Get Stock&error=" . $update_stock_query->errorInfo()[2]);
                }
            }        
        }
        
        function getAllStock() {
            $update_stock_query = $this->db->prepare("
                 SELECT * FROM `stock`;
            ");

            if ( $update_stock_query->execute() ) {
                 $stock = $update_stock_query->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js, "yes") == 0 ) {
                    echo json_encode($stock);
                }
                else { 
                    return $stock;
                }
            }
            else {
                if ( strcmp($this->js,"yes") == 0 ) {
                    echo "Failed to Get Stock\n" . $update_stock_query->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Get Stock&error=" . $update_stock_query->errorInfo()[2]);
                }
            }        
        }
       


        
        function getSets() {
            $params = array();
            if ( isset($this->data['type']) ) {

                if ( strcmp($this->data['type'], "core")==0 || 
                     strcmp($this->data['type'], "duel deck")==0 ||
                     strcmp($this->data['type'], "from the vault")==0 ) {

                    $params = array( ':type' => $this->data['type'] );

                    $get_sets_query = $this->db->prepare("
                            SELECT * FROM `sets` AS `a`
                                INNER JOIN `set_images` AS `b` ON `a`.`code` = `b`.`code`
                            WHERE `a`.`type` = :type
                            ORDER BY `a`.`releaseDate`
                    ");

                }
                else if ( strcmp($this->data['type'], "expansion")==0 ) {
                    $get_sets_query = $this->db->prepare("
                        SELECT * FROM `sets` AS `a`
                            INNER JOIN `set_images` AS `b` ON `a`.`code` = `b`.`code`
                        WHERE `type` = 'expansion' OR `type` = 'reprint' OR `type` = 'un'
                        ORDER BY `releaseDate`
                    ");
                }
                else if ( strcmp($this->data['type'], "multiplayer")==0 ) {
                    $get_sets_query = $this->db->prepare("
                        SELECT * FROM `sets` AS `a`
                            INNER JOIN `set_images` AS `b` ON `a`.`code` = `b`.`code`
                        WHERE `type` = 'planechase' OR `type` = 'commander' OR `type` = 'archenemy' OR `type` = 'conspiracy'
                        ORDER BY `releaseDate`
                    ");
                }
                else if ( strcmp($this->data['type'], "special")==0 ) {
                    $get_sets_query = $this->db->prepare("
                        SELECT * FROM `sets` AS `a`
                            INNER JOIN `set_images` AS `b` ON `a`.`code` = `b`.`code`
                        WHERE `type` = 'promo' OR `type` = 'box' OR `type` = 'masters' OR `type` = 'premium deck' OR `type` = 'starter'OR `type` = 'vanguard'
                        ORDER BY `releaseDate`
                    ");
                }                      

            }
            else {
                $get_sets_query = $this->db->prepare("
                        SELECT * FROM `sets` AS `a`
                            INNER JOIN `set_images` AS `b` ON `a`.`code` = `b`.`code`
                        ORDER BY `releaseDate`
                ");
            }

            if ( $get_sets_query->execute($params) ) {

                $set_array = $get_sets_query->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js, "yes")==0 ) {
                    echo json_encode($set_array);
                }
                else {
                    return $set_array;
                }
            }
            else {
                if ( strcmp($this->js, "yes")==0 ) {
                    echo "Failed to load Sets\n" . $get_sets_query->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Load Sets&error=" . $get_sets_query->errorInfo()[2]);
                }
            }                    

        }
                
        
        function getSetList() {
        
            if ( !isset($this->data['code']) ) {
                die('Set code Required' . "\n");
            }
            $params = array( ':set' => $this->data['code'] );
            $get_setlist = $this->db->prepare('
                SELECT * FROM `cards` WHERE `set` = :set
            ');
            if ( $get_setlist->execute($params) ) {
                $setlist = $get_setlist->fetchAll(PDO::FETCH_ASSOC);
                if ( strcmp($this->js, "yes")==0 ) {
                    echo json_encode($setlist);
                }
                else {
                    return $setlist;
                }
                
            }
            else {
                if ( strcmp($this->js, "yes")==0 ) {
                    echo "Failed to load Setlist\n" . $get_setlist->errorInfo()[2] . "\n";
                }
                else {
                    header("Location: http://magic.isleofgamesaz.com/admin/index.php?message=Failed to Load Setlist&error=" . $get_setlist->errorInfo()[2]);
                }
            }      
        }
        
        function getSetListData() {
            $cards = $this->getSetList();
            
            $conditions = $this->getConditions();
            
            $languages = $this->getLanguages();
            
            $groups = $this->getGroups();
            
            $setstock = $this->getSetStock();
            
            $this->data['foil'] = 0;
            $pricelist = $this->getPriceList();
            
            $prices = array();
            for ( $i = 0 ; $i < count($pricelist) ; $i++ ) {
                $prices[$pricelist[$i]['card_id']] = $pricelist[$i];
            }
            
            // Build Conditions Select Box
            $cond_select = '<select class="form-control input-sm cond_select" onchange="togglePrice($(this), $(this).closest(\'tr\').attr(\'id\'));">';
            if ( count($conditions) == 0 ) {
                $cond_select .= '<option value="1" selected>Near Mint</option></select>';
            }
            else {
                $cond_select .= '<option value="' . ((100 - $conditions[0]['percent'])/100) . '" selected>' . $conditions[0]['name'] . '</option>';
                for ( $i = 1 ; $i < count($conditions) ; $i++ ) {
                    $cond_select .= '<option value="' . ((100 - $conditions[$i]['percent'])/100) . '">' . $conditions[$i]['name'] . '</option>';
                }
                $cond_select .= '</select>';
            }
            
            
            // Build Languages Select Box
            $lang_select = '<select class="form-control input-sm lang_select" onchange="togglePrice($(this), $(this).closest(\'tr\').attr(\'id\'));">';
            if ( count($languages) == 0 ) {
                $lang_select .= '<option value="1" selected>English</option></select>';
            }
            else {
                $lang_select .= '<option value="' . (($languages[0]['percent'])/100 + 1) . '" selected>' . $languages[0]['name'] . '</option>';
                for ( $i = 1 ; $i < count($languages) ; $i++ ) {
                    $lang_select .= '<option value="' . (($languages[$i]['percent'])/100) + 1 . '">' . $languages[$i]['name'] + '</option>';
                }
                $lang_select .= '</select>';
            }
            
            // Build Groups Select Box
            $group_select = '<select class="form-control input-sm group_select" >';
            if ( count($groups) == 0 ) {
                $group_select .= '<option value="1" selected>Default</option></select>';
            }
            else {
                $group_select .= '<option value="' . $groups[0]['id'] . '" selected>' . $groups[0]['name'] . '</option>';
                for ( $i = 1 ; $i < count($groups) ; $i++ ) {
                    $group_select .= '<option value="' . $groups[$i]['id'] + '">' . $groups[$i]['name'] . '</option>';
                }
                $group_select .= '</select>';
            }
            
            
            
            
            
            $data = array( "data" => array() );
            foreach ( $cards as $index => $card ) {
                
                $stocktable = '<table cellpadding="5" cellspacing="0" border="0" style="float: right; padding-left:300px;">';
                $stocktable .= '<thead><tr><th>Condition</th><th>Language</th><th>Group</th><th>Stock<th>Price</th><th class="del_mod_buttons"></th></thead><tbody>';
                
                $stock_count = 0;
                foreach ( $setstock as $stock ) {
                    if ( $stock['card_id'] == $card['id'] ) {
                        $stock_count++;
                        if ( !isset($conditions[$stock['condition']-1]) ) {
                            $this_cond = "Undefined";
                        }
                        else {
                            $this_cond = $conditions[$stock['condition']-1]['name'];
                        }
                        
                        if ( !isset($languages[$stock['language']-1]) ) {
                            $this_lang = "Undefined";
                        }
                        else {
                            $this_lang = $languages[$stock['language']-1]['name'];
                        }
                        
                        if ( !isset($groups[$stock['group']-1]) ) {
                            $this_group = "Default";
                        }
                        else {
                            $this_group = $groups[$stock['group']-1]['name'];
                        }
                        
                        $checked = "";
                        if ( $stock['foil'] == 1 ) $checked = "checked";
                        
                        $stocktable .= '<tr><td class="inventory_condition">' . $this_cond . '</td><td class="inventory_language">' . $this_lang . '</td><td class="inventory_group"><div class="input-group" group="'. $this_group .'">' . $group_select . '</div></td>';
                        $stocktable .= '<td class="inventory_stock"><div class="input-group"><input class="form-control input-sm stock" type="number" min="1" value="' . $stock['stock'] . '" onkeydown="if (event.keyCode == 13) $(\'#modButton_' . $stock['id'] . '\').click()"></input></div></td>';
                        $stocktable .= '<td class="inventory_price"><div class="input-group"><span class="input-group-addon input-sm">$</span><input class="form-control input-sm price_input" type="text" value="' . $stock['price'] . '" onkeydown="if (event.keyCode == 13) $(\'#modButton_' . $stock['id'] . '\').click()"></input><span class="input-group-addon input-sm"><input type="checkbox" class="inventory_foil" aria-label="Foil" disabled ' . $checked . '> Foil</span></div></td>';
                        $stocktable .= '<td class="inv_form"><button class="delButton" id="delButton_'. $stock['id'] .'" onClick="deleteStock(\'' . $stock['id'] . '\', reloadSetList)">Delete</button><button class="modButton" id="modButton_'. $stock['id'] .'" onClick="modStock(\'' . $stock['id'] . '\', $(this), reloadSetList)">Modify</button></td></tr>';
                    }
                   
                }

                
                
                if ( $stock_count == 0 ) {
                    $stocktable = 'Out of Stock';
                    $in_stock = 0;
                }
                else {
                    $stocktable .= '</tbody></table>';
                    $in_stock = 1;
                }

                if ( !isset($prices[$card['id']]) ) { $price = 0; }  
                else { $price =  $prices[$card['id']]['fair_price']; }
            
           
                $data['data'][] = array(
                    "name" => '<span class="mtgcard">' . $card['name'] . '</span>',
                    "rarity" => $card['rarity'],
                    "conditions" => $cond_select,
                    "languages" => $lang_select,
                    "groups" => $group_select,
                    "stock" => '<div class="input-group"><input class="form-control input-sm stock_input" type="number" min="1" width="3" onkeydown="if (event.keyCode == 13) $(\'#addButton_' . $stock['id'] . '\').click()"></input></div>',
                    "price" => '<div class="input-group"><span class="input-group-addon input-sm">$</span><input class="form-control input-sm price_input" type="text" value="' . $price . '" onkeydown="if (event.keyCode == 13) $(\'#addButton_' . $card['id'] . '\').click()"></input><span class="input-group-addon input-sm"><input type="checkbox" name="new_foil" class="new_foil" onchange="togglePrice($(this), ' . $card['id'] . ');" aria-label="Foil"> Foil</span></div>',
                    "addbutton" => '<div class="input-group"><button class="addButton" id="addButton_' . $card['id'] . '" onClick="addStock (\''. $card['id'] .'\', $(this) , reloadSetList )" >Add</button></div>',
                    "inventory" => $stocktable,
                    "instock" => '<span class = "hidden">' . $in_stock . '</span>'
                );
                
            }
            
            echo json_encode($data);
            
        }
        
        
        function getSetName() {
            if ( !isset($this->data['code']) ) {
                die('Set code Required' . "\n");
            }
            
            $set_param = array(
                    ':set' => $this->data['code']
            );      

            $get_set_query = $this->db->prepare("
                SELECT name FROM sets
                    WHERE code = :set 
            ");

            $get_set_query->execute($set_param);
            $set = $get_set_query->fetch(PDO::FETCH_OBJ);

                    echo $set->name;

        }
        
        function getSetNames() {

            $get_sets_query = $this->db->prepare("
                SELECT * FROM sets
            ");

            $get_sets_query->execute();
            $sets_array = $get_sets_query->fetchAll();

            $sets = array();
            foreach ( $sets_array as $set ) {
                $sets[$set['code']] = $set;  
            }

            return $sets_array;
        }   
        
        function getSetData() {

            $set_param = array(
                    ':set' => $this->data['code']
            );      

            $get_set_query = $this->db->prepare("
                SELECT * FROM sets
                    WHERE code = :set 
            ");

            if ( $get_set_query->execute($set_param) ) {
                $set = $get_set_query->fetch(PDO::FETCH_ASSOC);
                echo json_encode($set);
            }
            else {
                echo "Failed to load Set Data\n" . $get_set_query->errorInfo()[2] . "\n";
            }
        }
        

        
        function getTable() {
            $id = "";
            if ( isset($this->data['id']) ) {
                $id = $this->data['id'];
            }
            
            $class = "";
            if ( isset($this->data['class']) ) {
                $class = $this->data['class'];
            }
            
            if ( !isset($this->data['headings']) ) {
                die('Headings Required' . "\n");
            }
            
            $table = '<table id="' . $id . '" class="' . $class . '">';
            $table .= '<tbody><tr>';
            foreach ( $this->data['headings'] as $heading ) {
                $table .= "<th>" . $heading . "</th>";
            }
            $table .= "</tr></tbody></table>";
            
            return $table;
            
        }
        
        
        function getSearchResult() {
            if ( !isset($this->data['query']) ) { die ('REQUIRED - "query"'); }
            $this->query = preg_replace('/[^a-zA-Z0-9Æ]/', '', $this->data['query']);
            
            // Load all the set data
            $get_sets_query = $this->db->prepare("
                    SELECT * FROM sets
            ");

            $get_sets_query->execute();
            $sets_array = $get_sets_query->fetchAll();

            $this->sets = array();
            foreach ( $sets_array as $set ) {
                    $this->sets[$set['code']] = $set;  
            }
            
            if ( strcmp($this->query, "ae") == 0 ) { 
                $params = array(
                        ':search' => "% " . $this->query . "%",
                        ':search2' => "% " . "Æ" . "%"
                );

                $get_search_query = $this->db->prepare("
                SELECT * FROM cards 
                        WHERE 
                        ( CONCAT(' ', cards.name) LIKE :search OR CONCAT(' ', cards.name) LIKE :search2 )
                        ORDER BY id DESC
                ");
	    }
            else {
        	//echo( "Searching for " . $this->query . "<br />");
		$this->query = str_replace("AE", "Æ", $this->query);
                $params = array(
                    ':search' => "% " . $this->query . "%"
		);

	    	$get_search_query = $this->db->prepare("
                    SELECT * FROM cards 
                            WHERE CONCAT(' ', cards.name) LIKE :search
                            ORDER BY id DESC
                ");
            }



            if (  !$get_search_query->execute($params) ) {
                die ('FAILED');
            }
            $this->result = $get_search_query->fetchAll();

            $this->beautify();
            
            if ( strcmp($this->js, "yes")==0 ) {
 
                $this->result = str_replace("`", "'", $this->result);
                echo json_encode ( array( "data" => $this->result) );
            }
            else {
                echo "Results: <br />";
                if ( isset($this->result) ) {
                    foreach ($this->result as $result) {
                        $cardname = utf8_decode($result['name']);
                        echo '<a href="">' . $cardname . '</a><br />';
                    }
                }
            }

        }
        



	function beautify() { 
		$size = count($this->result);
		for ( $j = 0 ; $j < $size ; $j++ ) {

			foreach ( $this->result[$j] as $key => $item) {
				if ( $item == NULL ) $this->result[$j][$key] = "-";
			}

			$this->result[$j]['set'] = $this->sets[ $this->result[$j]['set'] ]['name'];
			$this->result[$j]['manaCost'] = '<span class="hidden">(' . $this->result[$j]['cmc'] . ')</span>' . $this->result[$j]['manaCost'];

			$this->result[$j]['manaCost'] = str_replace('{W}', '<span class="mana mana-w"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U}', '<span class="mana mana-u"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B}', '<span class="mana mana-b"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R}', '<span class="mana mana-r"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G}', '<span class="mana mana-g"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{0}', '<span class="mana mana-0"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{1}', '<span class="mana mana-1"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2}', '<span class="mana mana-2"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{3}', '<span class="mana mana-3"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{4}', '<span class="mana mana-4"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{5}', '<span class="mana mana-5"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{6}', '<span class="mana mana-6"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{7}', '<span class="mana mana-7"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{8}', '<span class="mana mana-8"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{9}', '<span class="mana mana-9"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{10}', '<span class="mana mana-10"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{11}', '<span class="mana mana-11"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{12}', '<span class="mana mana-12"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{13}', '<span class="mana mana-13"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{14}', '<span class="mana mana-14"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{15}', '<span class="mana mana-15"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{16}', '<span class="mana mana-16"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{17}', '<span class="mana mana-17"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{18}', '<span class="mana mana-18"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{19}', '<span class="mana mana-19"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{20}', '<span class="mana mana-20"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{S}', '<span class="mana mana-s"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{X}', '<span class="mana mana-x"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/W}', '<span class="mana mana-2w"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/U}', '<span class="mana mana-2u"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/B}', '<span class="mana mana-2b"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/R}', '<span class="mana mana-2r"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/G}', '<span class="mana mana-2g"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/W}', '<span class="mana mana-uw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/U}', '<span class="mana mana-wu"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/W}', '<span class="mana mana-gw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/G}', '<span class="mana mana-wg"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/W}', '<span class="mana mana-bw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/B}', '<span class="mana mana-wb"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/W}', '<span class="mana mana-rw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/R}', '<span class="mana mana-wr"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/G}', '<span class="mana mana-ug"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/U}', '<span class="mana mana-gu"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/R}', '<span class="mana mana-ur"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/U}', '<span class="mana mana-ru"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/B}', '<span class="mana mana-ub"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/U}', '<span class="mana mana-bu"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/G}', '<span class="mana mana-bg"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/B}', '<span class="mana mana-gb"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/R}', '<span class="mana mana-br"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/B}', '<span class="mana mana-rb"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/G}', '<span class="mana mana-rg"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/R}', '<span class="mana mana-gr"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/W}', '<span class="mana mana-wp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/U}', '<span class="mana mana-up"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/B}', '<span class="mana mana-bp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/R}', '<span class="mana mana-rp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/G}', '<span class="mana mana-gp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P}', '<span class="mana mana-p"></span>', $this->result[$j]['manaCost']);

		}

	}






} // Class




?>