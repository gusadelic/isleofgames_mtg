<?php

class SearchResult
{
	var $db;
	var $query;
	var $js;
	var $sets;
	var $result;

	function __construct($query, $js, $command) {

		$this->js = $js;

		$this->query = preg_replace('/[^a-zA-Z0-9Æ]/', '', $query);

		// setup the DB
                require 'global.inc.php';
                $this->db = $db;

		// Load all the set data
                $get_sets_query = $this->db->prepare("
			SELECT * FROM sets
		");

		$get_sets_query->execute();
		$sets_array = $get_sets_query->fetchAll();

		$this->sets = array();
		foreach ( $sets_array as $set ) {
			$this->sets[$set['code']] = $set;  
		}


		switch ($command) {

			case "search":
						
				if ( strcmp($this->query, "ae") == 0 ) { 

					$params = array(
						':search' => "% " . $this->query . "%",
						':search2' => "% " . "Æ" . "%"
					);

					$get_search_query = $this->db->prepare("
			        	SELECT * FROM cards 
						WHERE 
						( CONCAT(' ', cards.name) LIKE :search OR CONCAT(' ', cards.name) LIKE :search2 )
						ORDER BY id DESC
			        ");
				}
				else {

					//echo( "Searching for " . $this->query . "<br />");

					$this->query = str_replace("AE", "Æ", $this->query);

					$params = array(
						':search' => "% " . $this->query . "%"
					);


			    	$get_search_query = $this->db->prepare("
			        	SELECT * FROM cards 
						WHERE CONCAT(' ', cards.name) LIKE :search
						ORDER BY id DESC
			       	 ");
				}


			   ;
			    if (  !$get_search_query->execute($params) ) {
			    	die ('FAILED');
			    }
			    $this->result = $get_search_query->fetchAll();

			    $this->beautify();
			    
				break;

			default:





		}





	} //Contructor


	function beautify() { 
		$size = count($this->result);
		for ( $j = 0 ; $j < $size ; $j++ ) {

			foreach ( $this->result[$j] as $key => $item) {
				if ( $item == NULL ) $this->result[$j][$key] = "-";
			}

			$this->result[$j]['set'] = $this->sets[ $this->result[$j]['set'] ]['name'];
			$this->result[$j]['manaCost'] = '<span class="hidden">(' . $this->result[$j]['cmc'] . ')</span>' . $this->result[$j]['manaCost'];

			$this->result[$j]['manaCost'] = str_replace('{W}', '<span class="mana mana-w"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U}', '<span class="mana mana-u"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B}', '<span class="mana mana-b"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R}', '<span class="mana mana-r"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G}', '<span class="mana mana-g"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{0}', '<span class="mana mana-0"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{1}', '<span class="mana mana-1"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2}', '<span class="mana mana-2"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{3}', '<span class="mana mana-3"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{4}', '<span class="mana mana-4"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{5}', '<span class="mana mana-5"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{6}', '<span class="mana mana-6"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{7}', '<span class="mana mana-7"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{8}', '<span class="mana mana-8"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{9}', '<span class="mana mana-9"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{10}', '<span class="mana mana-10"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{11}', '<span class="mana mana-11"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{12}', '<span class="mana mana-12"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{13}', '<span class="mana mana-13"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{14}', '<span class="mana mana-14"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{15}', '<span class="mana mana-15"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{16}', '<span class="mana mana-16"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{17}', '<span class="mana mana-17"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{18}', '<span class="mana mana-18"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{19}', '<span class="mana mana-19"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{20}', '<span class="mana mana-20"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{S}', '<span class="mana mana-s"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{X}', '<span class="mana mana-x"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/W}', '<span class="mana mana-2w"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/U}', '<span class="mana mana-2u"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/B}', '<span class="mana mana-2b"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/R}', '<span class="mana mana-2r"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{2/G}', '<span class="mana mana-2g"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/W}', '<span class="mana mana-uw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/U}', '<span class="mana mana-wu"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/W}', '<span class="mana mana-gw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/G}', '<span class="mana mana-wg"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/W}', '<span class="mana mana-bw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/B}', '<span class="mana mana-wb"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/W}', '<span class="mana mana-rw"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{W/R}', '<span class="mana mana-wr"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/G}', '<span class="mana mana-ug"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/U}', '<span class="mana mana-gu"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/R}', '<span class="mana mana-ur"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/U}', '<span class="mana mana-ru"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{U/B}', '<span class="mana mana-ub"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/U}', '<span class="mana mana-bu"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/G}', '<span class="mana mana-bg"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/B}', '<span class="mana mana-gb"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{B/R}', '<span class="mana mana-br"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/B}', '<span class="mana mana-rb"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{R/G}', '<span class="mana mana-rg"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{G/R}', '<span class="mana mana-gr"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/W}', '<span class="mana mana-wp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/U}', '<span class="mana mana-up"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/B}', '<span class="mana mana-bp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/R}', '<span class="mana mana-rp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P/G}', '<span class="mana mana-gp"></span>', $this->result[$j]['manaCost']);
			$this->result[$j]['manaCost'] = str_replace('{P}', '<span class="mana mana-p"></span>', $this->result[$j]['manaCost']);

		}

	}






} // Class




?>