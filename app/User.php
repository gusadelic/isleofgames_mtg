<?php

class User
{
	var $db;
	var $email;
	var $password;
	var $firstname;
        var $lastname;
        var $admin;
        var $avatar_card;
        
        // $user is an array which must contain 'email'
	function __construct($user) {
            if ( !isset($user['email']) ) {
                die("Constructor requires an Email Address!\n");
            }
            
            // setup the DB
                require 'global.inc.php';
                $this->db = $db;
                
                $this->email = $user['email'];
                
            //If the name is not set, get the existing account
            if ( !isset($user['firstname']) || !isset($user['lastname']) ) {
                
                $params = array( ':email' => $this->email );
                $get_user = $this->db->prepare("SELECT * FROM `users` WHERE `email_address` = :email");
                $get_user->execute($params);
                $user_data = $get_user->fetch(PDO::FETCH_ASSOC);
                
                $this->admin = $user_data['admin'];
                $this->avatar_card = $user_data['avatar_card'];
                $this->firstname = $user_data['first_name'];
                $this->lastname = $user_data['last_name'];
                $this->password = $user_data['password'];

            }
            
            // Else create a new account
            else {
		$this->admin = 0;
                $this->avatar_card = 0;
                $this->firstname = $user['firstname'];
                $this->lastname = $user['lastname'];
                
                $params = array(
                    ':email_address' => $this->email,
                    ':first_name' => $this->firstname,
                    ':last_name' => $this->lastname,
                    ':admin' => $this->admin,
                    ':avatar_card' => $this->avatar_card
                );
                
                $add_user = $this->db->prepare("
                INSERT INTO `users`(`admin`, `first_name`, `last_name`, `email_address`, `avatar_card`) 
                VALUES (:admin,:first_name,:last_name,:email_address,:avatar_card)
                ");
                
                if ( $add_user->execute($params) ) {
                    echo "User added!\n";
                }
                else {
                    echo "User Add Failed\n" . $add_user->errorInfo()[2] . "\n";
                }
            }
	} //Contructor
       


	function setPassword($password) {
            $this->password = hash('whirlpool', $password);
      	}
  
    function checkPassword($password) {
        if ( hash('whirlpool', $password) == $this->password ) {
            return true;
        }
        else { return false; }
        
    }
    
    function makeAdmin() {
        $this->admin = 1;
    }
    
    
    function updateUser() {
        $params = array(
            ':email_address' => $this->email,
            ':first_name' => $this->firstname,
            ':last_name' => $this->lastname,
            ':password' => $this->password,
            ':admin' => $this->admin,
            ':avatar_card' => $this->avatar_card
        );
        $update_user = $this->db->prepare("
           UPDATE `users` SET 
                `password`=:password,`admin`=:admin,`first_name`=:first_name,`last_name`=:last_name,`avatar_card`=:avatar_card
           WHERE `email_address` = :email_address     
        ");
        
        if ( $update_user->execute($params) ) {
            echo "User Updated!\n";
        }
        else {
            echo "Update Failed!\n" . $update_user->errorInfo()[2] . "\n";
        }
    }






} // Class




?>