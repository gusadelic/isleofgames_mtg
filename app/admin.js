function getCardList(json) {

    $("#dialog").append('<table id="resulttable" class="tablesorter"><thead><tr><th class="cardname">Card Name</th><th class="setname">Set</th><th class="condition">Condition</th><th class="stock">Stock</th><th class="price">Price</th><th></th></tr></thead><tbody></tbody></table>');
            $.each(json, function(index, value) {
				var price = "-";
				var condition = "-";
				var stock = "Out of stock";
				var id = "";
				var shipping = "0";

				var setname;
				var stock_object;

				$.ajax({
					url:'results.php?setname=' + json[index].set + '&javascript=yes&canary=' + encodeURIComponent($('#canary').val()),
					type: 'get',
					async: false,
					success: function(setname_result) { 
						setname = setname_result; 
					}
				});


				$("#resulttable").find('tbody').append('<tr class="card" id="' + json[index].id + '"></tr>');
			    $('tr#' + json[index].id).append('<td rowspan="1" class="cardname">' + json[index].name + '</td>');
			    $('tr#' + json[index].id).append('<td rowspan="1" class="setname" id="' + json[index].set + '">' + setname + '</td>');
			                    
			    var newrowtarget = 'tr#' + json[index].id;		

			 	$(newrowtarget).append('<td><select class="new_condition"><option value ="Near Mint" selected>NM</option><option value ="Slightly Played">SP</option><option value ="Moderately Played">MP</option><option value ="Heavily Played">HP</option></select>  <input type="checkbox" name="new_foil" class="new_foil">Foil</td>');
			    $(newrowtarget).append('<td><input class="new_stock" type="number" min="1"></input></td>');
			  	$(newrowtarget).append('<td>$<input class="new_price" type="text" value="' + price + '"></input></td>');
				$(newrowtarget).append('<td><input type="button" class="listbutton" onMouseDown="add_stock(\'' + json[index].id + '\')" onMouseUp="view_inventory()" value="Add"> </td>');

                $.ajax({
                    url: 'results.php?stock-card=' + json[index].id + '&javascript=yes&canary=' + encodeURIComponent($('#canary').val()),
                    type: 'get',
                    async: false,
                    success: function(stocks) { stock_object = JSON.parse(stocks); 

						$.each(stock_object, function(i, v) {
							if ( !stock_object[i].stock ) {
								stock = "Out of stock";	
							}
							else {
								stock = stock_object[i].stock;
								id = stock_object[i].item;
							}
						
							if ( stock_object[i].foil == 1 ) {
								condition = stock_object[i].condition + " - FOIL";
							}
							else {	
								condition = stock_object[i].condition;
							}
								
							
                            $('tr#' + json[index].id).find('td.cardname').attr('rowspan', i+2);
                            $('tr#' + json[index].id).find('td.setname').attr('rowspan', i+2);

							var newrowtarget = 'tr#card-' + stock_object[i].item;
							$('tbody').append('<tr id="card-' + stock_object[i].item + '"></tr>');
							$(newrowtarget).append('<td class="inventory_condition">' + condition + '</td>');

					        $(newrowtarget).append('<td><input class="inventory_stock" type="number" min="1"></input></td>');
	            			$(newrowtarget  + " input.inventory_stock").attr("value", stock_object[i].stock );
           		
            				$(newrowtarget).append('<td>$<input class="inventory_price" type="text"></input></td>');
	            			$(newrowtarget  + " input.inventory_price").attr("value", stock_object[i].price );

                            $(newrowtarget).append('<td class="inv_form"><input type="button" class="listbutton" onMouseDown="change_stock(\'' + stock_object[i].item + '\')" onMouseUp="view_inventory()" value="Modify"></input><input type="button" onMouseDown="delete_item(\'' + stock_object[i].item + '\')" onMouseUp="view_inventory()" value="Delete"></input> </td>');	

						});
					}
				});	    
       		});
}
       



    function show_inventory(user_id) {

	$("#result").html("<h1>Current Inventory</h1>");
	$("#result").append('<table id="resulttable" class="center"><tbody><tr><th>Item</th><th>Set</th><th>Condition</th><th>Quantity</th><th>Price</th><th> </th><th> </th></tr></tbody></table>');

					$.ajax({
                		url: 'admin.php',
                		type: 'POST',
                		data: 'cmd=inventory' + '&user_id=' + user_id + '&javascript=yes&canary=' + encodeURIComponent($('#canary').val()),
                   	dataType: 'JSON',
                		async: false,
                		success: function(data) {			

    						var total_value = 0.00;
 
		                    $.each(data, function(i, v) {
		                        var product = data[i].stock * data[i].price;
		                        var inventory_item = "card-" + data[i].item;
		                        $("#result tbody").append('<tr id="' + inventory_item + '"></tr>'); 
		                        $("#" + inventory_item  + "").append('<td class="cardname">' + data[i].name + '</td>');
		                        $("#" + inventory_item  + "").append('<td class="setname" id="' + data[i].set + '">' + data[i].set + '</td>');

		                        if ( data[i].foil == 1 ) {
		                        	$("#" + inventory_item  + "").append('<td>' + data[i].condition + ' FOIL</td>');
		                        }
		                        else {
		                        	$("#" + inventory_item  + "").append('<td>' + data[i].condition + '</td>');
		                        }
		                        
		                        $("#" + inventory_item  + "").append('<td><input class="inventory_stock" type="number" min="1"></input></td>');
		                        $("#" + inventory_item  + " input.inventory_stock").attr("value", data[i].stock );
		                        	
		                        $("#" + inventory_item  + "").append('<td>$<input class="inventory_price" type="text"></input></td>');
		                        $("#" + inventory_item  + " input.inventory_price").attr("value", data[i].price );

		                        $("#" + inventory_item  + "").append('<td>' + accounting.formatMoney(product) + '</td>');
		                        $("#" + inventory_item  + "").append('<td><input type="button" class="listbutton" onMouseDown="change_stock(\'' + data[i].item + '\')" onMouseUp="view_inventory()" value="Modify"></input><input type="button" onMouseDown="delete_item(\'' + data[i].item + '\')" onMouseUp="view_inventory()" value="Delete"></input> </td>');    
		                        total_value += product;
		                    });

                      
                      
                      		$('#result tbody').append('<tr class="total"><td> </td><td class="total_value" colspan="4">Total Value</td><td>' + accounting.formatMoney(total_value) + '</td><td> </td></tr>');

                			},

      					error:function(xhr, ajaxOptions, thrownError){
              				console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
              				if ( xhr.status = 200 ) {
              					$("#result tbody").append('<tr><td colspan="4">' + xhr.responseText + '</td></tr>');
              				}

              			}
             		});
        }

function change_stock(item, user_id) {

	var post_data = 'cmd=modify' + '&user_id=' + user_id + '&qty=' + $('#card-' + item + ' input.inventory_stock').val() + '&price=' + $('#card-' + item + ' input.inventory_price').val() + '&item=' + item + '&javascript=yes&canary=' + encodeURIComponent($('#canary').val());

	$.ajax({
		url: 'admin.php',
		type: 'POST',
		async: false,
		data: post_data,
		success: function(data) {

		},
		error:function(xhr, ajaxOptions, thrownError){
              				console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
              				if ( xhr.status = 200 ) {
              					$("#result tbody").append('<tr><td colspan="4">' + xhr.responseText + '</td></tr>');
              				}
		}
	});

}

function add_stock(card_id, user_id) {
	var is_foil = 0
	if ( $('tr#' + card_id + ' input.new_foil').is(":checked") ) { is_foil = 1; }

	var post_data = 'cmd=add' + '&user_id=' + user_id + '&name=' + $('tr#' + card_id + ' td.cardname').html() + '&set=' + $('tr#' + card_id + ' td.setname').attr('id') + '&condition=' + $('tr#' + card_id + ' .new_condition option:selected').html() + '&foil=' + is_foil + '&qty=' + $('tr#' + card_id + ' input.new_stock').val() + '&price=' + $('tr#' + card_id + ' input.new_price').val() + '&id=' + card_id + '&javascript=yes&canary=' + encodeURIComponent($('#canary').val());

	$.ajax({
 		url: 'admin.php',
		type: 'POST',
		async: false,
		data: post_data,
		success: function(data) {
			
		},
		error:function(xhr, ajaxOptions, thrownError){
              				console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
              				if ( xhr.status = 200 ) {
              					$("#result tbody").append('<tr><td colspan="4">' + xhr.responseText + '</td></tr>');
              				}
		}
	});

}

function delete_item(item_id, user_id) {

	var post_data = 'cmd=delete' + '&user_id=' + user_id + '&item=' + item_id + '&canary=' + encodeURIComponent($('#canary').val());

	$.ajax({
 		url: 'admin.php',
		type: 'POST',
		async: false,
		data: post_data,
		success: function(data) {
			
		},
		error:function(xhr, ajaxOptions, thrownError){
              				console.log('status = '+xhr.status+' thrownError = '+thrownError+ ' response = '+xhr.responseText);
              				if ( xhr.status = 200 ) {
              					$("#result tbody").append('<tr><td colspan="4">' + xhr.responseText + '</td></tr>');
              				}
		}
	});

}


function getPrice (name, set) {

					price_name = name.replace(/ /g,"_");
			
					if ( set.substring(0,2) === "M1" ) {
						price_set = set;
					}
					else {
						price_set = set.replace(/ /g, "_");
					}
				
					$.ajax({
						url:'getprice.php?set=' + price_set + '&name=' + price_name + '&canary=' + encodeURIComponent($('#canary').val()),
						type: 'get',
						async: false,
						success: function(result) {
							price = result;
						}
					});

				return price;

}


