<?php 
	session_start();

        require('MTGApp.php');
        

        if ( isset( $_GET['function'] ) ) {
                if ( strcmp($_GET['function'], "sets") == 0 ) {
                
                    $app = new MTGApp($_GET);
                    echo $app->getSets();
                }
                else if ( strcmp($_GET['function'], "setlist") == 0 ) {
                    $app = new MTGApp($_GET);
                    echo $app->getSetList();
                    
                }
                else if ( strcmp($_GET['function'], "cards") == 0 ) {

			if ( !isset($_GET['letter']) ) {

				$params = array(
					':letter' => "a%"
				);

				$get_cards_query = $db->prepare("
		    	    SELECT * FROM `cards` 
					WHERE `name` LIKE :letter
					GROUP BY `name`
					ORDER BY `name`
				");
			}
			else if ( strcmp($_GET['letter'], "other") == 0 ) {


				$params = array(
					':letter' => "^[^A-Za-z]"
					);

				$get_cards_query = $db->prepare("
		        	SELECT * FROM `cards` 
					WHERE `name` REGEXP :letter
					GROUP BY `name`
					ORDER BY `name`
				");

			}
			else { 


				$params = array(
				':letter' => $_GET['letter'] . "%"
				);

				$get_cards_query = $db->prepare("
			        SELECT * FROM `cards` 
					WHERE `name` LIKE :letter
					GROUP BY `name`
					ORDER BY `name`
				");
			}	

			$get_cards_query->execute($params);

			$card_array = $get_cards_query->fetchAll();


			echo ('<h1> Cards </h1>');

			echo ('<h5> -');

			if ( !isset($_GET['letter']) || $_GET['letter'] == "a" ) { echo ("A "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'a\')">A</a> '); } 
			if ( $_GET['letter'] == "b" ) { echo ("B "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'b\')</a> '); } 	
			if ( $_GET['letter'] == "c" ) { echo ("C "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'c\')">C</a> '); } 
			if ( $_GET['letter'] == "d" ) { echo ("D "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'d\')">D</a> '); } 
			if ( $_GET['letter'] == "e" ) { echo ("E "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'e\')">E</a> '); } 
			if ( $_GET['letter'] == "f" ) { echo ("F "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'f\')">F</a> '); } 
			if ( $_GET['letter'] == "g" ) { echo ("G "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'g\')">G</a> '); } 				
			if ( $_GET['letter'] == "h" ) { echo ("H "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'h\')">H</a> '); } 
			if ( $_GET['letter'] == "i" ) { echo ("I "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'i\')">I</a> '); } 
			if ( $_GET['letter'] == "j" ) { echo ("J "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'j\')">J</a> '); } 
			if ( $_GET['letter'] == "k" ) { echo ("K "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'k\')">K</a> '); } 
			if ( $_GET['letter'] == "l" ) { echo ("L "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'l\')">L</a> '); } 	
			if ( $_GET['letter'] == "m" ) { echo ("M "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'m\')">M</a> '); } 
			if ( $_GET['letter'] == "n" ) { echo ("N "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'n\')">N</a> '); } 
			if ( $_GET['letter'] == "o" ) { echo ("O "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'o\')">O</a> '); } 
			if ( $_GET['letter'] == "p" ) { echo ("P "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'p\')">P</a> '); } 
			if ( $_GET['letter'] == "q" ) { echo ("Q "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'q\')">Q</a> '); } 				
			if ( $_GET['letter'] == "r" ) { echo ("R "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'r\')">R</a> '); } 
			if ( $_GET['letter'] == "s" ) { echo ("S "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'s\')">S</a> '); } 
			if ( $_GET['letter'] == "t" ) { echo ("T "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'t\')">T</a> '); }
			if ( $_GET['letter'] == "u" ) { echo ("U "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'u\')">U</a> '); } 
			if ( $_GET['letter'] == "v" ) { echo ("V "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'v\')">V</a> '); } 	
			if ( $_GET['letter'] == "w" ) { echo ("w "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'w\')">W</a> '); } 
			if ( $_GET['letter'] == "x" ) { echo ("X "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'x\')">X</a> '); } 
			if ( $_GET['letter'] == "y" ) { echo ("Y "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'y\')">Y</a> '); } 
			if ( $_GET['letter'] == "z" ) { echo ("Z "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'z\')">Z</a> '); } 
			if ( $_GET['letter'] == "other" ) { echo ("Other "); } else { echo ('<a href="#" onclick="getCardAlphaList(\'other\')">Other</a> '); } 

			echo ('- </h5>');

			foreach ($card_array as $card) {
				echo ('<a href="browse.php?card=' . $card['name'] . '">' . $card['name'] . '</a><br />' );
			}

		}

		else {
			die ( "Invalid command!");
		}

	}


	else {
            echo '<div id="browse_container" class="panel panel-default" style="margin-left: 5%; width: 90%">
                    <div class="panel-heading">Browse</div>
                    <div class="panel-body" id="browse_content">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#expansions" data-toggle="tab">Expansions</a></li>
                        <li onclick="drawSets(\'core\', $(\'#core\') );"><a href="#core" data-toggle="tab">Core Sets</a></li>
                        <li onclick="drawSets(\'duel deck\', $(\'#duel_deck\') );"><a href="#duel_deck" data-toggle="tab">Duel Decks</a></li>
                        <li onclick="drawSets(\'from the vault\', $(\'#from_the_vault\') );"><a href="#from_the_vault" data-toggle="tab">From the Vault</a></li>
                        <li onclick="drawSets(\'multiplayer\', $(\'#multiplayer\') );"><a href="#multiplayer" data-toggle="tab">Multiplayer Sets</a></li>
                        <li onclick="drawSets(\'special\', $(\'#special\') );"><a href="#special" data-toggle="tab">Special Sets</a></li>                        
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="expansions">
                        </div>
                        <div class="tab-pane fade active in" id="core">
                        </div>
                        <div class="tab-pane fade active in" id="duel_deck">
                        </div>
                        <div class="tab-pane fade active in" id="from_the_vault">
                        </div>
                        <div class="tab-pane fade active in" id="multiplayer">
                        </div>
                        <div class="tab-pane fade active in" id="special">
                        </div>
                    </div>
                  </div>
            ';
            
            echo "<script>window.onload = function() { drawSets('expansion', $('#expansions') ); };</script>";

            echo '
            <div id="set_modal" class="modal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div id="modal_header" class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h2 class="modal-title"></h2>
                    </div>
                    <div id="modal_content" class="modal-body">
                      
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
            </div>
            ';
            
                
             
        }


?>

	