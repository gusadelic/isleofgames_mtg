var keyup = 0;
var tmp = null;


  var options = {
    widthFixed : true,
    showProcessing: true,
    headerTemplate : '{content} {icon}', // Add icon for jui theme; new in v2.7!

    widgets: [ 'stickyHeaders' ]

    };


$( document ).ready(function() {
	$("#javascript").val("yes");
	$("input").each(function(){
		$(this).attr("autocomplete", "off");
	});

	$("#search_results").tablesorter( );

	$("#search").keyup( function(event) {
		$('#search_results tbody').html("");

		keyup = 1;

		if ( tmp == null ) {
			countdown();
		}

	});

	$("#searchform").submit(function() {
		return false;
	});

});

function countdown() {
	if ( keyup == 0 ) {
		window.clearTimeout(tmp);
		tmp = null;
		search();
	}
	else {
		keyup--;
		tmp = window.setTimeout(countdown,600);
	}

}

function testSearch() {
	console.log("SEARCH!");
}


function search() {

	$.ajax({
		type: "GET",
		url: "results.php",
		async: false,
		data: $("#searchform").serialize()
	}).done( function(result) {  

		var json = JSON.parse(result);

		$('#search_results tbody').html("");

		$.each(json, function(index, value) {
        //                $("#results").append('<li class="result"><a href="results.php?card=' + json[index].name + '&javascript=no&canary=' + $('#canary').val() + '">' + json[index].name + '</a></li>');
			$("#search_results tbody").append('<tr><td class="card_name">' + json[index].name + '</td><td class="card_set">' + json[index].set + '</td><td class="card_type">' + json[index].type + '</td><td class="card_manacost">' + json[index].manaCost + '</td><td class="card_power">' + json[index].power + '</td><td class="card_toughness">' + json[index].toughness + '</td></tr>');
        });

		$("#results").css('z-index', 100);
		$("#results").css({border: '2px solid black'});
		$("#results").css({background: '#f1f1f1'});

		if ( $("#search").val() === "" ) {
				$("#results").css('z-index', -100);
				$("#results").css({border: '2px solid white'});
				$("#results").css({background: '#ffffff'});
                $("#results").html(""); 
        }
		else if ( !json[0] ) {
			$("#results").html("No Matches!");
		}

        // let the plugin know that we made a update 
		$("#search_results").trigger("update"); 
		// set sorting column and direction
		//var sorting = [[0,0]]; 
		// sort on the first column 
		//$("#search_results").trigger("sorton",[sorting]);

	});


/*
	$.get('results.php', $("#searchform").serialize(), function(result) {
	

	}); 
*/

}

/*
$(document).ready(function() 
    { 
    	getAllCards();
    	$("#search_results").tablesorter();
    } 
); 
*/

$(document).on("click", "li.result", function() {
        $("#search").val( $(this).html() );
	getSearchResult( $("#search").val() );
//  	$.get('browse.php', "card=" + $(this).html(), function(result) {
//		$('#dialog').html(result);
//	});
        $("#results").css('z-index', -100);
        //$("#results").css({border: '2px solid white'});
        //$("#results").css({background: '#ffffff'});
//        $("#results").html("");
});

$(document).on("click", "td.setname", function() {
	$(this).html('Loading, please wait...');
	getSetList($(this).attr('id'));
});
    
$(document).on("click", "td.cardname", function() {
	getCardResult( $(this).attr('id'), $(this).html() ); 
	//getSearchResult($(this).html());
	$(this).html('Loading, please wait...');
});

$(document).on("click", "td#home", function() {
	$("td#home").css('background-color', 'rgba(0,0,0,0)');
	$("td#home").css('color', 'white');
	$("td#home").css('cursor', 'default');
	window.location.href = 'index.php';
});

$(document).on("mouseenter", "td#store", function() {
	$("td#store").css('background-color', 'rgba(0,0,0,1)');
	$("td#store").css('color', 'red');
	$("td#store").css('cursor', 'pointer');
	$("div.dropdown#store").html('<li id="store1">Browse Sets</li><li id="store2">Browse Promo Sets</li><li id="store3">Browse Cards</li>');
});

$(document).on("mouseleave", "div.dropdown#store", function() {
	$("td#store").css('background-color', 'rgba(0,0,0,0)');
	$("td#store").css('color', 'white');
	$("td#store").css('cursor', 'default');
	$("div.dropdown#store").html('');
});

$(document).on("click", "li#store1", function() {
	$("td#store").css('background-color', 'rgba(0,0,0,0)');
	$("td#store").css('color', 'white');
	$("td#store").css('cursor', 'default');
	$("div.dropdown#store").html('');
	$.get('browse.php', "function=sets", function(result) {
		$('#result').html(result);
	});
});

$(document).on("click", "li#store2", function() {
	$("td#store").css('background-color', 'rgba(0,0,0,0)');
	$("td#store").css('color', 'white');
	$("td#store").css('cursor', 'default');
	$("div.dropdown#store").html('');
	$.get('browse.php', "function=promo", function(result) {
		$('#result').html(result);
	});
});

$(document).on("click", "li#store3", function() {
	$("td#store").css('background-color', 'rgba(0,0,0,0)');
	$("td#store").css('color', 'white');
	$("td#store").css('cursor', 'default');
	$("div.dropdown#store").html('');
	$.get('browse.php', "function=cards", function(result) {
		$('#result').html(result);
	});
});


$(document).on("click", function() {
	$("#menu td#store").css('background-color', 'rgba(0,0,0,0)');
	$("#menu td#store").css('color', 'white');
	$("#menu td#store").css('cursor', 'default');
	$("div.dropdown").html('');
})




